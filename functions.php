<?php

const ANSWER_DATABASE = __DIR__.'/data/questions.json';
const DEFAULT_SCORE   = 8;

function load_database(): array
{
    if (!file_exists(ANSWER_DATABASE)) {
        file_put_contents(ANSWER_DATABASE, json_encode([]));
    }

    return json_decode(file_get_contents(ANSWER_DATABASE), true);
}

function save_database(array $data, bool $backup = false)
{
    if ($backup) {
        copy(ANSWER_DATABASE, ANSWER_DATABASE.'-'.time().'.bak');
    }

    file_put_contents(ANSWER_DATABASE, json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE));
}

function get_tickets($filter, int $limit = 30): array
{
    switch ($filter) {
        case 'new':
            $set = get_new($limit);
            break;
        case 'difficult':
            $set = get_difficult($limit);
            break;
        case 'random':
        default:
            $set = get_random($limit);
    }

    return array_values($set);
}

function get_random(int $limit = 30): array
{
    $data = array_filter(load_database(), function ($element) {
        return $element['score'] > 1;
    });

    shuffle($data);

    return array_slice($data, 0, $limit);
}

function get_new(int $limit = 30): array
{
    $data = array_filter(load_database(), function ($element) {
        return $element['score'] > 1;
    });

    shuffle($data);
    usort(
        $data,
        function ($a, $b) {
            if (($a['show_count'] ?? 0) === ($b['show_count'] ?? 0)) {
                return 1;
            }

            return ($a['show_count'] ?? 0) > ($b['show_count'] ?? 0) ? 1 : -1;
        }
    );

    return array_slice($data, 0, $limit);
}

function reset_data()
{
    $data = load_database();

    foreach ($data as $item) {
        $data[$item['id']]['score']      = DEFAULT_SCORE;
        $data[$item['id']]['show_count'] = 0;
    }

    save_database($data, true);
}

function stats(int $limit = 30): array
{
    $data = load_database();

    $new       = 0;
    $difficult = 0;
    $hidden = 0;
    $correct = 0;

    foreach ($data as $item) {
        $score = $item['score'] ?? DEFAULT_SCORE;
        $count = $item['show_count'] ?? 0;

        if ($count === 0) {
            $new++;
        }

        if ($score > DEFAULT_SCORE) {
            $difficult++;
        }

        if ($score < DEFAULT_SCORE) {
            $correct++;
        }

        if ($score === 0) {
            $hidden++;
        }
    }

    return [
        'new' => $new,
        'correct' => $correct,
        'difficult' => $difficult,
        'hidden' => $hidden,
    ];
}

function get_difficult(int $limit = 30): array
{
    $data = load_database();
    shuffle($data);
    usort(
        $data,
        function ($a, $b) {
            if (($a['score'] ?? DEFAULT_SCORE) === ($b['score'] ?? DEFAULT_SCORE)) {
                return 1;
            }

            return ($a['score'] ?? DEFAULT_SCORE) > ($b['score'] ?? DEFAULT_SCORE) ? -1 : 1;
        }
    );

    return array_slice($data, 0, $limit);
}

function update($id, $result)
{
    $data = load_database();

    $score = $data[$id]['score'] ?? DEFAULT_SCORE;
    switch ($result) {
        case 'correct':
            $score = $score / 2;
            break;
        case 'wrong':
            $score = $score * 4;
            break;
        case 'repeat':
            $score = $score * 2;
            break;
        case 'no-repeat':
            $score = 0;
            break;
    }

    $data[$id]['score']      = $score;
    $data[$id]['show_count'] = ($data[$id]['show_count'] ?? 0) + 1;

    save_database($data);
}
