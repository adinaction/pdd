<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="apple-itunes-app" content="app-id=1532078416">
    <title>მართვის მოწმობის გამოცდა ახალი ტესტებით - თეორია.On.ge</title>
    <meta property="og:type" content="article"/>
    <meta property="og:url" content="https://teoria.on.ge/exam"/>
    <meta property="og:title" content="მართვის მოწმობის გამოცდა ახალი ტესტებით - თეორია.On.ge"/>
    <meta property="og:image" content="https://teoria.on.ge/image/og/og-exam.jpg"/>
    <meta property="og:description"
          content="მართვის მოწმობის ახალი ტესტები. დაიხმარეთ თეორიის გამოცდის ყველაზე სრულყოფილი სავარჯიშო საქართველოში!"/>
    <meta property="fb:app_id" content="534503209985887"/>
    <link rel="icon" href="https://teoria.on.ge/favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="https://teoria.on.ge/favicon.ico" type="image/x-icon"/>
    <script>
        var TEORIA_FILES_PREFIX = 'new/';
        var SERVER_TIMESTAMP = 1643114926;
        var USER_ID = 0    </script>
    <script type="text/javascript" src="//static.on.ge/global/assets/js/ads.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/bootstrap.min.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/jquery.tooltipster.min.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/jquery.magnific-popup.min.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/jquery.scrollup.min.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/chart.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/cookie.jquery.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/fontfaceonload.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/onge.pivot.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/global.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/helpers.js?v=1.53"></script>
    <script type="text/javascript" src="assets/exam.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/exam-end.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/user.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/dropdown-hover.js?v=1.53"></script>
    <script type="text/javascript" src="https://teoria.on.ge/js/map.js?v=1.53"></script>
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/bootstrap.min.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/bootstrap-theme.min.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/tooltipster.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/jquery.magnific-popup.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/global.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/global.components.css?v=1.53"/>
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/user.css?v=1.53"/>
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/test_exam.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/static.css?v=1.53">
    <link type="text/css" rel="stylesheet" href="https://teoria.on.ge/css/static-extra.css?v=1.53">

    <link type="text/css" rel="stylesheet" href="https://account.on.ge/css/onbar-2.css?v=20170827">
    <!--    <script type="text/javascript" src="https://account.on.ge/js/onbar.js?v=20160511"></script>-->
</head>

<body class="user-anon">


<section class="content-wrapper">
    <div class="container">
        <div class="row">
            <?php include 'functions.php'; $stats = stats(); ?>
            <span class="label-wrap text-info">Решено: <?php print($stats['correct']+$stats['difficult'])?>;</span>
            <span class="label-wrap text-info">Осталось: <?php print($stats['new'])?>;</span>
            <span class="label-wrap text-success">Правильных: <?php print($stats['correct'])?>;</span>
            <span class="label-wrap text-danger">Сложных: <?php print($stats['difficult'])?>;</span>
            <span class="label-wrap">Скрытых: <?php print($stats['hidden'])?>;</span>
        </div>
    </div>

</section>
<section class="content-wrapper">
    <div class="exam-global-wrap">
        <div class="container">

            <div class="start-page">

                <div class="row">
                    <div class="start-page-choose-category start-page-wrap">

                        <div class="lang-tip-text spo-label">
                            ბილეთების ენა და ფერი დააყენეთ მთავარი მენიუს მარჯვნივ, დროშასთან.
                        </div>

                        <div class="start-page-object spo-category">
                            <h3 class="spo-label">
                                მართვის მოწმობის კატეგორია:
                                <span class="more-info">
							<a href="/info/categories"><span class="glyphicon glyphicon-info-sign"></span> კატეგორიების შესახებ</a>
						</span>
                            </h3>
                            <div class="exam-category-selector">
                                <div class="exam-category-list inner">
                                    <div data-category="1" data-category-name="A, A1" data-category-key="A"
                                         class="exam-category exam-cat-1 ">
                                        <div class="svg-wrapper">

                                        </div>
                                        <span class="label-wrap ">A, A1: მოტოციკლი, მსუბუქი მოტოციკლი</span>
                                    </div>
                                    <div data-category="2" data-category-name="B, B1" data-category-key="B"
                                         class="exam-category exam-cat-2 active">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0.3 33.9 99.4 36.7" enable-background="new 0.3 33.9 99.4 36.7"
                                                 xml:space="preserve">
<g id="Layer_1_1_" display="none">
    <g id="St9YL3_1_" display="inline" opacity="0.39">
    </g>
</g>
                                                <g id="Layer_2">
                                                    <path d="M15.5,55.4c-4.2,0-7.6,3.4-7.6,7.6s3.4,7.6,7.6,7.6c4.2,0,7.6-3.4,7.6-7.6S19.7,55.4,15.5,55.4z M15.5,66.2
		c-1.8,0-3.2-1.4-3.2-3.2s1.4-3.2,3.2-3.2c1.8,0,3.2,1.4,3.2,3.2C18.7,64.7,17.3,66.2,15.5,66.2z"/>
                                                    <path d="M82.5,55.4c-4.2,0-7.6,3.4-7.6,7.6c0,4.2,3.4,7.6,7.6,7.6c4.2,0,7.6-3.4,7.6-7.6C90.2,58.8,86.7,55.4,82.5,55.4z
		 M82.5,66.2c-1.8,0-3.2-1.4-3.2-3.2s1.4-3.2,3.2-3.2s3.2,1.4,3.2,3.2S84.3,66.2,82.5,66.2z"/>
                                                    <path d="M78.2,44.5c0,0-18.8-8.8-24.8-10c-6-1.3-26.6-0.1-28.6,1.2l-11.5,5.6L2.1,46.8L0.3,57.5c0,0-0.1,6.3,0.4,7.8h5.5L6.3,63
		c0-5.1,4.1-9.2,9.2-9.2c5.1,0,9.2,4.1,9.2,9.2l0.2,2.2h48.4l0-2.8c0-5.1,3.9-8.9,9-8.9s9.5,3.8,9.5,8.9L92,65c0,0,5.8,0.3,7.7-0.4
		c0,0,0.7-9.8-2.9-13.4C93.6,48.3,87.2,46.4,78.2,44.5z M26,45.9c0,0,2.4-5,4.9-6.9c1.9-1.6,12.4-1.6,12.4-1.6l0.4,9.7L26,45.9z
		 M71.6,48.2l-24.7-1l-0.6-9.8c9.2-0.3,19.6,4.5,25.6,8.1C77.9,48.9,71.6,48.2,71.6,48.2z"/>
                                                </g>
</svg>
                                        </div>
                                        <span class="label-wrap ">B, B1: მსუბუქი ავტომობილი, კვადროციკლი</span>
                                    </div>
                                    <div data-category="3" data-category-name="C" data-category-key="C"
                                         class="exam-category exam-cat-3 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0.2 32.9 99.5 32.2" enable-background="new 0.2 32.9 99.5 32.2"
                                                 xml:space="preserve">
<g>
    <path d="M23.5,61.1c0-1.5-1.2-2.7-2.7-2.7s-2.7,1.2-2.7,2.7c0,1.5,1.2,2.7,2.7,2.7S23.5,62.5,23.5,61.1L23.5,61.1z M13.1,61.1
		c0-1.5-1.2-2.7-2.7-2.7c-1.5,0-2.7,1.2-2.7,2.7c0,1.5,1.2,2.7,2.7,2.7C11.9,63.8,13.1,62.5,13.1,61.1L13.1,61.1z M91.9,45.4
		c0,2.7,2.1,2.8,5.9,4l1.4,0.4v-6.4c0-0.8-0.6-1.4-1.4-1.4h-5.9V45.4L91.9,45.4z M72.6,61.1c0-2.7-2.2-4.9-4.9-4.9
		c-0.9,0-1.7,0.2-2.4,0.6c-1.5,0.8-2.5,2.4-2.5,4.3c0,0.5,0.1,0.9,0.2,1.3h-1.1v-5.6l-1.9,0v5.6H26.6v-1.2h32.2V58H26.6v-1.2h-3.4
		c1.5,0.8,2.5,2.4,2.5,4.3v0.1h-0.8c0,2.2-1.8,4-4.1,4c-2.2,0-4-1.8-4.1-4h-0.9v-0.1c0-1.8,1-3.5,2.5-4.3h-5.6
		c1.5,0.8,2.5,2.4,2.5,4.3l0,0.1h-0.8c0,2.2-1.8,4-4.1,4c-2.2,0-4-1.8-4.1-4H5.5v-0.1c0-1.8,1-3.5,2.5-4.3H0.2V32.9h83.2v23.9l1.4,0
		v-1.9V35.8C85.3,35.9,93,38,93,38c4.3,1.4,6.7,1.5,6.7,4.6v11.7c0,0.4-0.3,1-0.9,1.4c0.6,0.5,0.9,1,0.9,1.4v2.3V60
		c0,0.8-1.1,2.2-3.8,2.4c0.1-0.4,0.2-0.9,0.2-1.3c0-2.7-2.2-4.9-4.9-4.9c-2.7,0-4.9,2.2-4.9,4.9c0,0.5,0.1,0.9,0.2,1.3h-0.8h-0.3
		h-0.5H72.4C72.5,62,72.6,61.5,72.6,61.1L72.6,61.1z M93.9,61.1c0-1.5-1.2-2.7-2.7-2.7c-1.5,0-2.7,1.2-2.7,2.7
		c0,1.5,1.2,2.7,2.7,2.7C92.7,63.8,93.9,62.5,93.9,61.1L93.9,61.1z M95.3,61.1c0,2.2-1.8,4.1-4.1,4.1c-2.2,0-4.1-1.8-4.1-4.1
		c0-2.2,1.8-4.1,4.1-4.1C93.5,57,95.3,58.8,95.3,61.1L95.3,61.1z M70.4,61.1c0-1.5-1.2-2.7-2.7-2.7c-1.5,0-2.7,1.2-2.7,2.7
		c0,1.5,1.2,2.7,2.7,2.7S70.4,62.5,70.4,61.1L70.4,61.1z M71.7,61.1c0,2.2-1.8,4.1-4.1,4.1s-4.1-1.8-4.1-4.1c0-2.2,1.8-4.1,4.1-4.1
		C69.9,57,71.7,58.8,71.7,61.1L71.7,61.1z"/>
</g>
</svg>
                                        </div>
                                        <span class="label-wrap ">C: სატვირთო ავტომობილი</span>
                                    </div>
                                    <div data-category="4" data-category-name="C1" data-category-key="C1"
                                         class="exam-category exam-cat-4 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 23.6 100 52.8" enable-background="new 0 23.6 100 52.8"
                                                 xml:space="preserve">
<g>
    <path d="M71.6,67.6c0,4.9,3.9,8.8,8.8,8.8c4.9,0,8.8-3.9,8.8-8.8c0-4.9-3.9-8.8-8.8-8.8C75.5,58.8,71.6,62.7,71.6,67.6z M76.3,67.6
		c0-2.2,1.8-4.1,4.1-4.1c2.2,0,4.1,1.8,4.1,4.1c0,2.2-1.8,4.1-4.1,4.1C78.1,71.7,76.3,69.8,76.3,67.6z M9.4,67.6
		c0,4.9,3.9,8.8,8.8,8.8s8.8-3.9,8.8-8.8c0-4.9-3.9-8.8-8.8-8.8C13.4,58.8,9.4,62.7,9.4,67.6z M14.2,67.6c0-2.2,1.8-4.1,4.1-4.1
		c2.2,0,4.1,1.8,4.1,4.1c0,2.2-1.8,4.1-4.1,4.1C16,71.7,14.2,69.8,14.2,67.6z M0.1,65.9c0,0.1,0,0.1,0,0.2c0.2,0.7,0.4,0.9,1.6,1.2
		c0.8,0.2,3.7,0.9,6,1.4c0-0.3-0.1-0.7-0.1-1c0-5.9,4.8-10.6,10.6-10.6c5.9,0,10.6,4.8,10.6,10.6c0,0.9-0.1,1.8-0.3,2.6
		c8.8,0,33.6,0.1,41.6,0.1c-0.2-0.9-0.3-1.8-0.3-2.7c0-5.9,4.8-10.6,10.6-10.6S91,61.7,91,67.6c0,0.6-0.1,1.2-0.2,1.8
		c0.3,0.1,0.7,0.1,1.2,0.1c1.7,0,4.3-0.3,5.2-0.7c1.7-0.6,3.3-2,2.6-7.5c-0.1-0.7-0.2-1.3-0.2-2c-0.5-4.6-0.8-7.9-6.6-10.4
		c-6.3-2.7-7.2-3.1-7.2-3.1c0,0-0.2-0.1-0.2-0.1l-0.3-0.1c0,0-14.8-15.8-17.3-17.8c-2.4-2-6.3-4.3-11.1-4.3c0,0-51.5,0-51.6,0
		c-0.4,0-1.3,0.2-1.8,2c-0.6,2.1-3.1,18.5-3.2,21.8C0.1,49.9-0.1,65.2,0.1,65.9z M89,50.5c0.1-0.2,0.3-0.4,0.6-0.4c0,0,0.1,0,0.1,0
		c5.4,1,6.4,3.6,7.2,7.5c0,0.2,0,0.4-0.1,0.5c-0.1,0.1-0.3,0.2-0.5,0.2H94c-0.3,0-0.6-0.2-0.6-0.5l0-0.1c-0.4-2.8-0.7-4.6-4.1-6.4
		C89,51.1,88.9,50.8,89,50.5z M57.5,52.5c0-1.2,1-2.2,2.3-2.2H63c1.2,0,2.2,1,2.2,2.2s-1,2.2-2.2,2.2h-3.2
		C58.5,54.7,57.5,53.7,57.5,52.5z M56.2,41.9c-0.5,0-1.2-0.6-2.2-5.3c-1.1-5.3-0.8-7.1-0.7-7.4c0-0.1,0-0.1,0.1-0.2
		c0.1-0.2,0.3-0.3,1.5-0.4c0.9,0,2.1,0,3.7,0c4.5,0.2,4.9,0.6,5,0.7c0.4,0.3,1.9,1.2,6.1,5.7c5.8,6.3,7.6,9.2,7.6,9.3
		c0.1,0.1,0.1,0.3,0.1,0.4c-0.1,0.1-0.2,0.2-0.3,0.2c0,0,0,0,0,0c-0.1,0-5.4-0.4-13.2-1.7c-3-0.5-5.2-0.9-6.5-1.2
		C56.7,42.1,56.4,42,56.2,41.9z M48.2,52.5c0-1.2,1-2.2,2.2-2.2h3.2c1.2,0,2.2,1,2.2,2.2s-1,2.2-2.2,2.2h-3.2
		C49.2,54.7,48.2,53.7,48.2,52.5z M7.1,46.9c0-1.2,1-2.2,2.2-2.2h25c1.2,0,2.2,1,2.2,2.2c0,1.2-1,2.2-2.2,2.2h-25
		C8.2,49.1,7.1,48.1,7.1,46.9z"/>
</g>
</svg>
                                        </div>
                                        <span class="label-wrap ">C1: მცირე სატვირთო ავტომობილი</span>
                                    </div>
                                    <div data-category="5" data-category-name="D" data-category-key="D"
                                         class="exam-category exam-cat-5 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 24.1 100 51" enable-background="new 0 24.1 100 51"
                                                 xml:space="preserve">
<circle cx="83" cy="68" r="7.2"/>
                                                <circle cx="16.9" cy="68" r="7.2"/>
                                                <path d="M0,30.1l0,37.1c0,2.2,1.8,4,4,4h3.5C7.2,70.2,7,69.1,7,68c0-5.5,4.5-9.9,9.9-9.9c5.5,0,9.9,4.4,9.9,9.9
	c0,1.1-0.2,2.2-0.5,3.2h47.3c-0.3-1-0.5-2.1-0.5-3.2c0-5.5,4.4-9.9,9.9-9.9c5.5,0,9.9,4.4,9.9,9.9c0,1.1-0.2,2.2-0.5,3.2h3.3
	c2.1,0,3.9-1.6,4-3.7c0.9-13.3,0.1-25.7-3.2-36.7c-1.2-3.9-4.8-6.6-8.9-6.6H6C2.7,24.1,0,26.8,0,30.1z M76,28.3h11.6
	c2.2,0,4.2,1.5,4.9,3.6c1.6,5.6,2.7,11.6,3.1,18.3H81.9L76,45.8V28.3z M53,28.3h19V45H53V28.3z M28,28.3h21V45H28V28.3z M4.2,29.9
	c0-0.9,0.7-1.6,1.6-1.6H24V45H4.2V29.9z"/>
</svg>
                                        </div>
                                        <span class="label-wrap ">D: ავტობუსი</span>
                                    </div>
                                    <div data-category="6" data-category-name="D1" data-category-key="D1"
                                         class="exam-category exam-cat-6 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Your_Icon" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="2 28.4 96.4 41.6" enable-background="new 2 28.4 96.4 41.6"
                                                 xml:space="preserve">
<path d="M77.1,64.2c0,3.2,2.6,5.8,5.8,5.8c3.2,0,5.8-2.6,5.8-5.8c0-3.2-2.6-5.8-5.8-5.8C79.7,58.3,77.1,61,77.1,64.2z M79.9,64.2
	c0-1.7,1.3-3,3-3c1.7,0,3,1.3,3,3s-1.3,3-3,3C81.2,67.2,79.9,65.8,79.9,64.2z M22,64.2c0,3.2,2.6,5.8,5.8,5.8s5.8-2.6,5.8-5.8
	c0-3.2-2.6-5.8-5.8-5.8C24.6,58.3,22,61,22,64.2z M24.8,64.2c0-1.7,1.3-3,3-3s3,1.3,3,3s-1.3,3-3,3C26.2,67.2,24.8,65.8,24.8,64.2z
	 M2,29.9v30.2c0,0.8,0.7,1.5,1.5,1.5l15.9,1.6h1.2c0.5-3.6,3.5-6.4,7.3-6.4c3.7,0,6.8,2.8,7.3,6.4h24.7H63h12.6
	c0.5-3.6,3.5-6.4,7.3-6.4c3.8,0,6.9,2.9,7.3,6.5l3,0c4,0,5.3-3.3,5.3-7.3c0-2.4-1.1-4.5-2.9-5.8L74.6,33.7c0,0-6.6-5.3-11.6-5.3H3.5
	C2.7,28.4,2,29.1,2,29.9z M94,51.5c0.2-0.6,0.5-0.6,1.6,0.2c1.2,0.9,1.6,3,1.7,3.5c0.1,0.5-0.4,0.9-1,0.9c-0.6,0-1.4,0-1.8-0.7
	C94.3,54.8,93.9,52.1,94,51.5z M66.2,44.4v-8.9c0-0.8,0.7-1.5,1.5-1.5h2.5c0,0,2.2-0.2,4.8,2c2.4,2,7.1,5.7,9.7,7.8
	c1.3,1,0.5,2.1-0.3,2.1H67.7C66.9,45.9,66.2,45.3,66.2,44.4L66.2,44.4z M40.6,44.4v-8.9c0-0.8,0.7-1.5,1.5-1.5h17.9
	c0.8,0,1.5,0.7,1.5,1.5v8.9c0,0.8-0.7,1.5-1.5,1.5H42C41.2,45.9,40.6,45.3,40.6,44.4L40.6,44.4z M17.4,35.5c0-0.8,0.7-1.5,1.5-1.5
	h17.9c0.8,0,1.5,0.7,1.5,1.5v8.9c0,0.8-0.7,1.5-1.5,1.5H18.9c-0.8,0-1.5-0.7-1.5-1.5C17.4,44.4,17.4,35.5,17.4,35.5z M4.3,35.5
	c0-0.8,0.7-1.5,1.5-1.5h7.9c0.8,0,1.5,0.7,1.5,1.5v8.9c0,0.8-0.7,1.5-1.5,1.5H5.7c-0.8,0-1.5-0.7-1.5-1.5V35.5z"/>
</svg>
                                        </div>
                                        <span class="label-wrap ">D1: მიკროავტობუსი</span>
                                    </div>
                                    <div data-category="7" data-category-name="T, S" data-category-key="TS"
                                         class="exam-category exam-cat-7 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Calque_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0.8 15.6 100 71" enable-background="new 0.8 15.6 100 71"
                                                 xml:space="preserve">
<g>
    <path d="M22,57.1c-4.5,0-8.2,3.7-8.2,8.2s3.7,8.2,8.2,8.2c4.5,0,8.2-3.7,8.2-8.2C30.2,60.8,26.6,57.1,22,57.1z"/>
    <path d="M22,44C10.3,44,0.8,53.6,0.8,65.3C0.8,77,10.3,86.6,22,86.6c11.7,0,21.3-9.5,21.3-21.3C43.3,53.6,33.8,44,22,44z M22,77.3
		c-6.6,0-12-5.4-12-12s5.4-12,12-12s12,5.4,12,12S28.6,77.3,22,77.3z"/>
    <path d="M85.6,56.3c-8.4,0-15.1,6.8-15.1,15.1c0,8.4,6.8,15.2,15.1,15.2c8.4,0,15.2-6.8,15.2-15.2C100.8,63.1,94,56.3,85.6,56.3z
		 M85.6,78.6c-3.9,0-7.1-3.2-7.1-7.1c0-3.9,3.2-7.1,7.1-7.1c3.9,0,7.1,3.2,7.1,7.1C92.7,75.4,89.5,78.6,85.6,78.6z"/>
    <path d="M73.3,57.7c-3.2-2.5-8.3-5.5-15.2-7v-1.9c7.8,1.6,13.4,5,16.7,7.7c3-2.2,6.7-3.5,10.7-3.5c2.2,0,4.3,0.4,6.2,1.1
		c-0.2-4.7-0.8-9.8-2.4-10.3c-7.8-2.9-17.9-4.6-26.1-5.4V23.3c0-2.1,1.1-2.6,1.2-2.7c1-0.3,1.6-1.3,1.3-2.3c-0.3-1-1.3-1.6-2.3-1.3
		c-1.5,0.4-4,2.2-4,6.3v14.8c-0.6,0-1.1-0.1-1.7-0.1l-4.4-19c-0.2-0.7-0.8-1.3-1.5-1.4c-0.9-0.2-22.5-3.9-38.8-0.6
		c-1,0.2-1.7,1.2-1.5,2.2c0.2,1,1.2,1.7,2.2,1.5c0.2,0,0.5-0.1,0.7-0.1l-3,17.4c0,0,0,0.1,0,0.1h-0.7c-3.6,0-4.3,2.3-4.3,2.3
		l-2.2,5.5l1.8,0.9C10.5,43,16,40.9,22,40.9c13.5,0,24.5,11,24.5,24.4c0,2.4-0.4,4.6-1,6.8h21.8c0-0.2,0-0.5,0-0.8
		C67.2,66,69.6,61.1,73.3,57.7z M80.6,44.5c0.5-0.2,1,0,1.2,0.5l1.5,3.9c0.2,0.5-0.1,1-0.5,1.2c-0.1,0-0.2,0.1-0.3,0.1
		c-0.4,0-0.7-0.2-0.9-0.6L80,45.7C79.8,45.2,80.1,44.6,80.6,44.5z M76.1,44.5c0.5-0.2,1,0,1.2,0.5l1.5,3.9c0.2,0.5-0.1,1-0.5,1.2
		c-0.1,0-0.2,0.1-0.3,0.1c-0.4,0-0.7-0.2-0.9-0.6l-1.5-3.9C75.4,45.2,75.6,44.6,76.1,44.5z M31.7,38.9c-1.5-0.5-2.9-0.8-4.2-0.9
		H15.4l2.8-16.3h13.5V38.9z M54.4,53.5l-6.9,0c-2.3-6.1-7.2-10.5-12-13V21.7h14.7l4.2,18L54.4,53.5L54.4,53.5L54.4,53.5z"/>
</g>
</svg>
                                        </div>
                                        <span class="label-wrap ">T, S: სასოფლო-სამეურნეო და საგზაო-სამშენებლო მანქანები</span>
                                    </div>
                                    <div data-category="8" data-category-name="Tram" data-category-key="TRAM"
                                         class="exam-category exam-cat-8 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 25 100 50" enable-background="new 0 25 100 50"
                                                 xml:space="preserve">
<path d="M97.1,43.6c-0.3-1.1-1.3-1.9-2.4-1.9H70h-8.3l-1.2-1.2c-0.3-0.3-0.8-0.5-1.2-0.5h-1h-1.6h-5l8.3-5.8l-7.9-5.8v-2.5
	c0-0.5-0.4-0.8-0.8-0.8h-2.5c-0.5,0-0.8,0.4-0.8,0.8v2.5L40,34.2l8.3,5.8h-5h-1.6h-1c-0.4,0-0.9,0.2-1.2,0.5l-1.2,1.2H30H5.3
	c-1.1,0-2.1,0.8-2.4,1.9L0,55.1v15h8.3v1.7h5l0,0c0,1.8,1.5,3.2,3.3,3.2c1.8,0,3.3-1.4,3.3-3.2l0.1,0h5l0,0c0,1.8,1.5,3.2,3.3,3.2
	c1.8,0,3.3-1.4,3.3-3.2h36.7c0,1.8,1.5,3.2,3.3,3.2c0,0,0.1,0,0.1,0c1.8,0,3.2-1.5,3.2-3.2l0.1,0h5l0,0c0,1.8,1.5,3.2,3.3,3.2
	c0,0,0.1,0,0.1,0c1.8,0,3.2-1.5,3.2-3.2l0.1,0h5v-1.7h8.3v-15L97.1,43.6z M10.4,53.8L10.4,53.8c0,0.7-0.6,1.3-1.3,1.3H5.1H5
	c-0.9,0-1.6-0.8-1.6-1.7c0-0.1,0-0.3,0.1-0.4L5,46.8c0.2-0.5,0.7-0.9,1.2-0.9h3c0.7,0,1.3,0.6,1.3,1.3V53.8z M20,67.1
	c0,0.7-0.6,1.2-1.3,1.2h-4.2c-0.7,0-1.3-0.5-1.3-1.2v-20c0-0.7,0.6-1.3,1.3-1.3l4.2,0.1c0.7,0,1.3,0.5,1.3,1.2V67.1z M28.2,67.1
	c0,0.7-0.5,1.2-1.2,1.2h-4.2c-0.7,0-1.2-0.5-1.2-1.2v-20c0-0.7,0.5-1.3,1.2-1.3l4.2,0.1c0.7,0,1.2,0.5,1.2,1.2V67.1z M41.9,53.8
	c0,0.7-0.6,1.3-1.3,1.3l-8.3,0c0,0-0.1,0-0.1,0c-0.7,0-1.2-0.6-1.2-1.3v-6.7c0-0.7,0.5-1.3,1.2-1.3c0,0,0.1,0,0.1,0l8.3,0.1
	c0.7,0,1.3,0.5,1.3,1.2V53.8z M42.1,34.2l7.9-5.8l7.9,5.8L50,39.7L42.1,34.2z M55.4,53.8c0,0.7-0.6,1.3-1.3,1.3h-8.3
	c-0.7,0-1.3-0.6-1.3-1.3v-6.7c0-0.7,0.6-1.3,1.3-1.3l8.3,0.1c0.7,0,1.3,0.5,1.3,1.2V53.8z M69,53.8c0,0.7-0.6,1.3-1.3,1.3l-8.3,0
	c0,0-0.1,0-0.1,0c-0.7,0-1.2-0.6-1.2-1.3v-6.7c0-0.7,0.5-1.3,1.2-1.3c0,0,0.1,0,0.1,0l8.3,0.1c0.7,0,1.3,0.5,1.3,1.2V53.8z
	 M78.2,67.1c0,0.6-0.5,1.1-1.1,1.2H73c0,0-0.1,0-0.1,0c-0.7,0-1.2-0.5-1.2-1.2v-20c0-0.7,0.5-1.3,1.2-1.3c0,0,0.1,0,0.1,0l4.2,0.1
	c0.6,0,1.1,0.6,1.1,1.2V67.1z M86.7,67.1c0,0.7-0.6,1.2-1.3,1.2h-4.2c-0.7,0-1.3-0.5-1.3-1.2v-20c0-0.7,0.6-1.3,1.3-1.3l4.2,0.1
	c0.7,0,1.3,0.5,1.3,1.2V67.1z M94.9,55.1h-4.1c-0.7,0-1.3-0.6-1.3-1.3h0v-6.7c0-0.7,0.6-1.3,1.3-1.3h3c0.6,0,1.1,0.4,1.2,0.9
	l1.4,6.2c0,0.1,0.1,0.3,0.1,0.4C96.5,54.3,95.8,55.1,94.9,55.1z"/>
</svg>
                                        </div>
                                        <span class="label-wrap ">Tram: ტრამვაი</span>
                                    </div>
                                    <div data-category="9" data-category-name="B+C1 Mil" data-category-key="MIL"
                                         class="exam-category exam-cat-9 ">
                                        <div class="svg-wrapper">
                                            <svg version="1.1" id="Layer_1" xmlns="http://www.w3.org/2000/svg"
                                                 xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                                                 viewBox="0 143.1 942.4 273.9"
                                                 enable-background="new 0 143.1 942.4 273.9" xml:space="preserve">
<g id="Layer_2">
    <path d="M328.7,306.2c-30.6,0-55.4,24.8-55.4,55.4s24.8,55.4,55.4,55.4c30.6,0,55.4-24.8,55.4-55.4S359.3,306.2,328.7,306.2z
		 M328.7,384.9c-13.1,0-23.3-10.2-23.3-23.3s10.2-23.3,23.3-23.3c13.1,0,23.3,10.2,23.3,23.3C352,374,341.8,384.9,328.7,384.9z"/>
    <path d="M816.7,306.2c-30.6,0-55.4,24.8-55.4,55.4s24.8,55.4,55.4,55.4c30.6,0,55.4-24.8,55.4-55.4
		C872.8,331,847.3,306.2,816.7,306.2z M816.7,384.9c-13.1,0-23.3-10.2-23.3-23.3s10.2-23.3,23.3-23.3c13.1,0,23.3,10.2,23.3,23.3
		S829.8,384.9,816.7,384.9z"/>
    <path d="M785.4,226.8c0,0-136.9-64.1-180.6-72.8c-43-9.5-193-0.7-207.6,8l-83.8,41.5l-82.3,40.1L218,321.5c0,0-0.7,45.9,2.9,56.8
		H261l0.7-16c0-37.1,29.9-67,67-67s67,29.9,67,67l1.5,16h352.5v-20.4c0-37.1,28.4-64.8,65.6-64.8c37.1,0,69.2,27.7,69.2,64.8
		l1.5,18.9c0,0,42.2,2.2,56.1-2.9c0,0,5.1-71.4-21.1-97.6C897.6,254.5,851,240.7,785.4,226.8z M405.2,237c0,0,17.5-36.4,35.7-50.3
		c13.8-11.7,90.3-11.7,90.3-11.7l2.9,70.7L405.2,237z M737.3,253.8l-179.9-7.3l-4.4-71.4c67-2.9,142.8,32.8,186.5,58.3
		C783.2,258.9,737.3,253.8,737.3,253.8z"/>
</g>
                                                <polygon
                                                        points="0,376.9 95.4,331 190.1,376.9 190.1,301.9 95.4,256 0,301.9 "/>
                                                <polygon
                                                        points="0,264 95.4,218.1 190.1,264 190.1,189 95.4,143.1 0,189 "/>
</svg>
                                        </div>
                                        <span class="label-wrap small-font-size">B+C1 Mil: B და C1 კატეგორია სამხედრო მოსამსახურეთათვის</span>
                                    </div>
                                </div>
                                <span class="caret"></span>
                            </div>
                        </div>

                        <div class="start-page-object spo-rate">
                            <h3 class="spo-label">Рейтинг вопросов:</h3>
                            <div class="exam-rate-selector">
                                <div class="exam-category-list inner">
                                    <div data-category="new" data-category-name="new" data-category-key="new"
                                         class="exam-rate exam-rate-new ">
                                        <div class="svg-wrapper">

                                        </div>
                                        <span class="label-wrap ">Новые</span>
                                    </div>
                                    <div data-category="difficult" data-category-name="difficult" data-category-key="difficult"
                                         class="exam-difficult exam-rate-difficult ">
                                        <div class="svg-wrapper">

                                        </div>
                                        <span class="label-wrap ">Сложные</span>
                                    </div>
                                    <div data-category="random" data-category-name="random" data-category-key="random"
                                         class="exam-random exam-rate-random ">
                                        <div class="svg-wrapper">

                                        </div>
                                        <span class="label-wrap ">В перемежку</span>
                                    </div>
                                </div>
                                <span class="caret"></span>
                            </div>
                        </div>

                        <div class="start-page-object spo-topics">
                            <h3 class="spo-label">
                                <span class="toggle-topics"><span class="glyphicon glyphicon-ok-sign"></span> ყველას ჩართვა/გამორთვა</span>
                                კითხვების თემები:
                            </h3>
                            <div class="exam-topics-selector">
                                <label title="მძღოლი, მგზავრი და ქვეითი, ნიშნები, კონვეცია" class="exam-topic"
                                       data-id="1"><input type="checkbox" name="topics[1]" class="topics-input"
                                                          value="1" checked>&nbsp; 1. მძღოლი, მგზავრი და ქვეითი,
                                    ნიშნები, კონვეცია</label>
                                <label title="უწესივრობა და მართვის პირობები" class="exam-topic" data-id="2"><input
                                            type="checkbox" name="topics[2]" class="topics-input" value="2" checked>&nbsp;
                                    2. უწესივრობა და მართვის პირობები</label>
                                <label title="მაფრთხილებელი ნიშნები" class="exam-topic" data-id="3"><input
                                            type="checkbox" name="topics[3]" class="topics-input" value="3" checked>&nbsp;
                                    3. მაფრთხილებელი ნიშნები</label>
                                <label title="პრიორიტეტის ნიშნები" class="exam-topic" data-id="4"><input type="checkbox"
                                                                                                         name="topics[4]"
                                                                                                         class="topics-input"
                                                                                                         value="4"
                                                                                                         checked>&nbsp;
                                    4. პრიორიტეტის ნიშნები</label>
                                <label title="ამკრძალავი ნიშნები" class="exam-topic" data-id="5"><input type="checkbox"
                                                                                                        name="topics[5]"
                                                                                                        class="topics-input"
                                                                                                        value="5"
                                                                                                        checked>&nbsp;
                                    5. ამკრძალავი ნიშნები</label>
                                <label title="მიმთითებელი ნიშნები" class="exam-topic" data-id="6"><input type="checkbox"
                                                                                                         name="topics[6]"
                                                                                                         class="topics-input"
                                                                                                         value="6"
                                                                                                         checked>&nbsp;
                                    6. მიმთითებელი ნიშნები</label>
                                <label title="საინფორმაციო-მაჩვენებელი ნიშნები" class="exam-topic" data-id="7"><input
                                            type="checkbox" name="topics[7]" class="topics-input" value="7" checked>&nbsp;
                                    7. საინფორმაციო-მაჩვენებელი ნიშნები</label>
                                <label title="სერვისის ნიშნები" class="exam-topic" data-id="8"><input type="checkbox"
                                                                                                      name="topics[8]"
                                                                                                      class="topics-input"
                                                                                                      value="8" checked>&nbsp;
                                    8. სერვისის ნიშნები</label>
                                <label title="დამატებითი ინფორმაციის ნიშნები" class="exam-topic" data-id="9"><input
                                            type="checkbox" name="topics[9]" class="topics-input" value="9" checked>&nbsp;
                                    9. დამატებითი ინფორმაციის ნიშნები</label>
                                <label title="შუქნიშნის სიგნალები" class="exam-topic" data-id="10"><input
                                            type="checkbox" name="topics[10]" class="topics-input" value="10" checked>&nbsp;
                                    10. შუქნიშნის სიგნალები</label>
                                <label title="მარეგულირებლის სიგნალები" class="exam-topic" data-id="11"><input
                                            type="checkbox" name="topics[11]" class="topics-input" value="11" checked>&nbsp;
                                    11. მარეგულირებლის სიგნალები</label>
                                <label title="სპეციალური სიგნალის გამოყენება" class="exam-topic" data-id="12"><input
                                            type="checkbox" name="topics[12]" class="topics-input" value="12" checked>&nbsp;
                                    12. სპეციალური სიგნალის გამოყენება</label>
                                <label title="საავარიო შუქური სიგნალიზაცია" class="exam-topic" data-id="13"><input
                                            type="checkbox" name="topics[13]" class="topics-input" value="13" checked>&nbsp;
                                    13. საავარიო შუქური სიგნალიზაცია</label>
                                <label title="სანათი ხელსაწყოები, ხმოვანი სიგნალი" class="exam-topic"
                                       data-id="14"><input type="checkbox" name="topics[14]" class="topics-input"
                                                           value="14" checked>&nbsp; 14. სანათი ხელსაწყოები, ხმოვანი
                                    სიგნალი</label>
                                <label title="მოძრაობა, მანევრირება, სავალი ნაწილი" class="exam-topic"
                                       data-id="15"><input type="checkbox" name="topics[15]" class="topics-input"
                                                           value="15" checked>&nbsp; 15. მოძრაობა, მანევრირება, სავალი
                                    ნაწილი</label>
                                <label title="გასწრება შემხვედრის გვერდის ავლით" class="exam-topic" data-id="16"><input
                                            type="checkbox" name="topics[16]" class="topics-input" value="16" checked>&nbsp;
                                    16. გასწრება შემხვედრის გვერდის ავლით</label>
                                <label title="მოძრაობის სიჩქარე" class="exam-topic" data-id="17"><input type="checkbox"
                                                                                                        name="topics[17]"
                                                                                                        class="topics-input"
                                                                                                        value="17"
                                                                                                        checked>&nbsp;
                                    17. მოძრაობის სიჩქარე</label>
                                <label title="სამუხრუჭე მანძილი, დისტანცია" class="exam-topic" data-id="18"><input
                                            type="checkbox" name="topics[18]" class="topics-input" value="18" checked>&nbsp;
                                    18. სამუხრუჭე მანძილი, დისტანცია</label>
                                <label title="გაჩერება დგომა" class="exam-topic" data-id="19"><input type="checkbox"
                                                                                                     name="topics[19]"
                                                                                                     class="topics-input"
                                                                                                     value="19" checked>&nbsp;
                                    19. გაჩერება დგომა</label>
                                <label title="გზაჯვარედინის გავლა" class="exam-topic" data-id="20"><input
                                            type="checkbox" name="topics[20]" class="topics-input" value="20" checked>&nbsp;
                                    20. გზაჯვარედინის გავლა</label>
                                <label title="რკინიგზის გადასასვლელი" class="exam-topic" data-id="21"><input
                                            type="checkbox" name="topics[21]" class="topics-input" value="21" checked>&nbsp;
                                    21. რკინიგზის გადასასვლელი</label>
                                <label title="მოძრაობა ავტომაგისტრალზე" class="exam-topic" data-id="22"><input
                                            type="checkbox" name="topics[22]" class="topics-input" value="22" checked>&nbsp;
                                    22. მოძრაობა ავტომაგისტრალზე</label>
                                <label title="საცხოვრებელი ზონა, სამარშრუტოს პრიორიტეტი" class="exam-topic"
                                       data-id="23"><input type="checkbox" name="topics[23]" class="topics-input"
                                                           value="23" checked>&nbsp; 23. საცხოვრებელი ზონა, სამარშრუტოს
                                    პრიორიტეტი</label>
                                <label title="ბუქსირება" class="exam-topic" data-id="24"><input type="checkbox"
                                                                                                name="topics[24]"
                                                                                                class="topics-input"
                                                                                                value="24" checked>&nbsp;
                                    24. ბუქსირება</label>
                                <label title="სასწავლო სვლა" class="exam-topic" data-id="25"><input type="checkbox"
                                                                                                    name="topics[25]"
                                                                                                    class="topics-input"
                                                                                                    value="25" checked>&nbsp;
                                    25. სასწავლო სვლა</label>
                                <label title="გადაზიდვები, ხალხი, ტვირთი" class="exam-topic" data-id="26"><input
                                            type="checkbox" name="topics[26]" class="topics-input" value="26" checked>&nbsp;
                                    26. გადაზიდვები, ხალხი, ტვირთი</label>
                                <label title="ველოსიპედი, მოპედი და პირუტყვის გადარეკვა" class="exam-topic"
                                       data-id="27"><input type="checkbox" name="topics[27]" class="topics-input"
                                                           value="27" checked>&nbsp; 27. ველოსიპედი, მოპედი და პირუტყვის
                                    გადარეკვა</label>
                                <label title="საგზაო მონიშვნა" class="exam-topic" data-id="28"><input type="checkbox"
                                                                                                      name="topics[28]"
                                                                                                      class="topics-input"
                                                                                                      value="28"
                                                                                                      checked>&nbsp; 28.
                                    საგზაო მონიშვნა</label>
                                <label title="სამედიცინო დახმარება" class="exam-topic" data-id="29"><input
                                            type="checkbox" name="topics[29]" class="topics-input" value="29" checked>&nbsp;
                                    29. სამედიცინო დახმარება</label>
                                <label title="მოძრაობის უსაფრთხოება" class="exam-topic" data-id="30"><input
                                            type="checkbox" name="topics[30]" class="topics-input" value="30" checked>&nbsp;
                                    30. მოძრაობის უსაფრთხოება</label>
                                <label title="ადმინისტრაციული კანონი" class="exam-topic" data-id="31"><input
                                            type="checkbox" name="topics[31]" class="topics-input" value="31" checked>&nbsp;
                                    31. ადმინისტრაციული კანონი</label>
                                <label title="ეკო-მართვა" class="exam-topic" data-id="32"><input type="checkbox"
                                                                                                 name="topics[32]"
                                                                                                 class="topics-input"
                                                                                                 value="32" checked>&nbsp;
                                    32. ეკო-მართვა</label>
                                <div class="all-cover">
                                    <span class="glyphicon glyphicon-ok-sign"></span> ყველა
                                </div>
                            </div>
                        </div>


                        <div class="start-page-object spo-questions">
                            <h3 class="spo-label">კითხვების რაოდენობა:</h3>
                            <div class="question-quantity-input-wrapper">
                                <input id="question-quantity" name="question-quantity" value="30" type="text"
                                       maxlength="2" onkeypress="return event.charCode >= 48 && event.charCode <= 57">
                                <span class="ticket">ბილეთი</span>
                            </div>
                            <span class="quantity-description">გამოცდისას ბარდება 30-ბილეთიანი ტესტი</span>
                            <label class="all-questions">
                                <input type="checkbox" name="all-questions" id="all-questions" value="1">
                                ყველა კითხვა არჩეული თემებიდან (მაქს. 150)
                            </label>
                        </div>


                        <div class="start-page-object spo-tournament tournament">
                            <label><input id="challenge" type="checkbox" value="gogo" name="chall" checked>
                                <span class="checkbox-text">შევეჯიბროთ სხვა მომხმარებლებს</span></label>
                        </div>


                        <div class="start-page-object spo-start-button">
                            <div class="start-page-button">
                                <button type="submit" id="start">Начать тест</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div style="display: none" class="questions-running-wrapper">
                <div class="row">
                    <div id="question" class="col-xs-12 image-container">

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-7 left-column">
                        <div class="box">
                            <span class="box-values" id="true">0</span>
                            <span class="label">სწორი</span>
                        </div>
                        <div class="box">
                            <span class="box-values" id="false">0</span>
                            <span class="label">შეცდომა</span>
                        </div>
                        <div class="box">
                            <div id="ticket-number"><span id="ticket-number-now">1</span>/<span
                                        id="ticket-number-sum"></span></div>
                            <span class="label">ბილეთი #</span>
                        </div>
                        <div class="box timer-box">
                            <span class="box-values" id="timer">00:00:00</span>
                            <span class="label">გასული დრო</span>
                        </div>
                    </div>
                    <div class="col-xs-5 right-column">
                        <button class="omit-button">Пропустить</button>
                        <button class="next-button">Дальше</button>
                        <label class="automatic-next">
                            <input id="no-repeat" type="checkbox">
                            Не показывать
                        </label>
                        <label class="show-correct">
                            <input id="repeat" type="checkbox">
                            Повторять чаще
                        </label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="questions-bullet-table">
                            <tbody>
                            <tr id="question-bullets-td">

                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-6 bottom-about">
                        <span>კლავიატურით მართვა: აკრიფეთ პასუხის შესაბამისი ციფრი და დააჭირეთ Enter-ს. ბილეთის ბოლოსთვის მოტოვება &mdash; ღილაკი Space.</span>
                    </div>
                    <div class="col-xs-2 col-xs-offset-4 stop-button">
                        <button id="stop-test">ტესტის შეწყვეტა</button>
                    </div>
                </div>
            </div>


            <div id="exam_end" class="exam_end" style="display: none;">

                <div class="pivot pivot-desktop" data-slot="#T-03" style="margin-top: 20px;">
                    <div class="admixer-ad admixer-ad-delayed"
                         id='admixer_70174509739641c98e36fa1987fc90d5_zone_79742_sect_28562_site_23193'
                         data-sender='admixer'></div>
                </div>

                <script type="text/javascript" src="http://w.sharethis.com/button/buttons.js"></script>
                <script type="text/javascript">stLight.options({
                        publisher: "92c38c2f-1d09-4960-8e7f-e1a3e9cca036",
                        doNotHash: true,
                        doNotCopy: true,
                        hashAddressBar: false,
                        shorten: false
                    });</script>
                <div class="row">
                    <h1 class="end-test-title">
                        Экзамен окончен
                    </h1>
                </div>

                <div class="row">
                    <div class="col-xs-7 left-column">
                        <div class="box">
                            <span class="label">სწორი</span>
                            <span class="box-values" id="true">11</span>
                        </div>
                        <div class="box">
                            <span class="label">შეცდომა</span>
                            <span class="box-values" id="false">10</span>
                        </div>
                        <div class="box">
                            <span class="label">ბილეთი #</span>

                            <div id="ticket-number"><span id="ticket-number-now">21</span>/<span
                                        id="ticket-number-sum">30</span></div>
                        </div>
                        <div class="box timer-box">
                            <span class="label">გასული დრო</span>
                            <span class="box-values" id="timer">10:10:10</span>
                        </div>
                    </div>
                    <div class="col-xs-5 right-column">
                        <div class="share-to-friends">
                            გაუზიარეთ მეგობრებს!
                        </div>
                        <div class="share-buttons-wrapper">
                            <span class='sharethis_fb'></span>
                            <span class='sharethis_tw'></span>
                            <span class='sharethis_gp'></span>
                            <span class='sharethis_in'></span>
                            <span class='sharethis_pn'></span>
                            <span class='sharethis_ok'></span>
                        </div>

                    </div>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <table class="questions-bullet-table">
                            <tbody>
                            <tr>
                                <td class="bullet-true"></td>
                                <td class="bullet-true"></td>
                                <td class="bullet-true"></td>
                                <td class="bullet-false"></td>
                                <td class="bullet-false"></td>
                                <td class="bullet-current"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row">
                    <h3 class="test-result-message">ჩაიჭერი!</h3>
                    <div class="test-result-actions">
                        <button id="replay-test">ახლიდან დაწყება იგივე ბილეთებით</button>
                        <button id="replay-new-test">ახალი გამოცდა სხვა ბილეთებით</button>
                    </div>
                    <div class="exams-history">
                        <a href="/tests/history">ჩემი გამოცდების ისტორია</a>
                    </div>
                    <div class="exams-results-info">
                        <span>შეცდომების და სწორი პასუხების სანახავად, გადაატარეთ მაუსი ფერადი უჯრების ზოლზე ან დააჭირეთ <a
                                    class="results-final-link" href="#">ამ ბმულს</a>.</span>
                    </div>
                </div>
            </div>

            <div class="exam-loader">
                <div class="spinner">
                    <div class="bounce1"></div>
                    <div class="bounce2"></div>
                    <div class="bounce3"></div>
                </div>
            </div>

        </div> <!-- end .container -->
    </div> <!-- end .exam-global-wrap -->
</section>

</body>
</html>
