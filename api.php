<?php
header('Content-Type: application/json');
header('Content-Type: application/json; charset=utf-8');
include 'functions.php';

$action = $_REQUEST['action'];

switch ($action) {
    case 'reset':
        reset_data();
        print(json_encode('[]', JSON_UNESCAPED_UNICODE));
        break;
    case 'stats':
        print(json_encode(stats(), JSON_UNESCAPED_UNICODE));
        break;
    case 'tickets':
        $filter = $_GET['filter'] ?? 'new';
        $limit = $_GET['limit'] ?? 30;

        print(json_encode(get_tickets($filter , $limit), JSON_UNESCAPED_UNICODE));
        break;
    case 'score':
        $id = $_POST['id'];
        update($id, $_POST['rate']);
        print(json_encode('[]', JSON_UNESCAPED_UNICODE));
        break;
}



