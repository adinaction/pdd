<?php

include 'functions.php';

while (true) {
    parse();
    sleep(array_rand(range(2, 10)));
}

function parse()
{
    $data = load_database();

    $params = [
        'cat_id'        => 2,
        'limit'         => 30,
        'topics'        => 'all',
        'all_questions' => false,
    ];

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, 'https://teoria.on.ge/tickets');
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'accept-language: en-US,en;q=0.9,ru;q=0.8',
        'content-type: application/x-www-form-urlencoded; charset=UTF-8',
        'cookie: exam-settings=%7B%22category%22%3A2%2C%22locale%22%3A%22ru%22%2C%22skin%22%3A%22dark%22%2C%22user%22%3A0%2C%22created%22%3A1643115277%2C%22questions%22%3A30%2C%22challenge%22%3Afalse%2C%22all_questions%22%3Afalse%2C%22topics%22%3A%5B%221%22%2C%222%22%2C%223%22%2C%224%22%2C%225%22%2C%226%22%2C%227%22%2C%228%22%2C%229%22%2C%2210%22%2C%2211%22%2C%2212%22%2C%2213%22%2C%2214%22%2C%2215%22%2C%2216%22%2C%2217%22%2C%2218%22%2C%2219%22%2C%2220%22%2C%2221%22%2C%2222%22%2C%2223%22%2C%2224%22%2C%2225%22%2C%2226%22%2C%2227%22%2C%2228%22%2C%2229%22%2C%2230%22%2C%2231%22%2C%2232%22%5D%2C%22autoShowCorrect%22%3Atrue%2C%22autoNextStep%22%3Afalse%7D; ',
    ]);
    curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    $response = curl_exec($ch);

    curl_close($ch);

    $results = json_decode($response);
    $counter = 0;
    if (json_last_error() === JSON_ERROR_NONE) {
        $summary = array_pop($results);

        foreach ($results as $result) {
            $score = 0;

            if (!array_key_exists($result->id, $data)) {
                $counter++;
            } else {
                $score = $data[$result->id]['score'] ?? 0;
            }

            $data[$result->id] = [
                'id'             => $result->id ?? null,
                'topic'          => $result->topic ?? null,
                'filename'       => $result->filename ?? null,
                'question'       => $result->question ?? null,
                'answers'        => json_decode($result->answers, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE),
                'desc'           => $result->desc ?? null,
                'cutoff'         => $result->cutoff ?? null,
                'timestamp'      => $result->timestamp ?? null,
                'correct_answer' => $result->correct_answer ?? null,
                'coefficient'    => $result->coeficient ?? null,
                'file_parent'    => $result->file_parent ?? null,
                'image'          => $result->image ?? null,
                'score'          => $score,
            ];
        }

        save_database($data);

        print_r(sprintf('Added %d new questions. Total - %d %s', $counter, count($data), "\n"));
    }
}
